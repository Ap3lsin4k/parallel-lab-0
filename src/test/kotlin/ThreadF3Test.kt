import theodorko.ThreadF1
import theodorko.ThreadF3
import kotlin.concurrent.thread
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

internal class ThreadF3Test {

    private val testSample: ThreadF3 = ThreadF3()
    private val test1: ThreadF1 = ThreadF1()

    @Test
    fun testSum() {
        val expected = 42

        assertEquals(testSample.Func3(), 3)
    }

    @Test
    fun testFunc1() {
        // A, B, C are vectors
        // MA, MD are matrices (NxN)
        // MA * MD is matrix multiplication
        // D * MB is multiplication of vector and matrix
        val a = intArrayOf()
        val b = intArrayOf()
        val c = intArrayOf()
        val d = intArrayOf()

        val ma = Array(1) {Array(1) {0} }
        val mb = Array(1) {Array(1) {0} }
        val md: Array<Array<Int>> = Array(1) {Array(1) {0} }
        test1.Func1(a, b, c, ma, md, d, mb)
    }

    @Test
    fun testMatrixMul1x1() {
        val data = theodorko.Data()
        val ma = Array(1) { IntArray(1) }
        val mb = arrayOf(intArrayOf(1))
        assertEquals(1, ma.size)
        assertEquals(mb.size, ma.size)
        assertContentEquals(arrayOf(intArrayOf(0))[0], data.multiply(ma, mb)[0])
    }

    @Test
    fun testMatrixMul3x2() {
        val data = theodorko.Data(2)
        val ma = Array(3) { IntArray(1) {1} }
        val mb = arrayOf(intArrayOf(2, 4))
        assertEquals(3, ma.size)
        val actual = data.multiply(ma, mb)
        assertEquals(actual.size, 3)
        assertContentEquals(intArrayOf(2, 4), actual[0])
        assertContentEquals(intArrayOf(2, 4), actual[1])
        assertContentEquals(intArrayOf(2, 4), actual[2])
        thread()
    }

}