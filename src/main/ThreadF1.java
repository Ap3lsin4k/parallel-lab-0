// створення  потокового класу для функції  F1
public class ThreadF1 extends Thread {
    //  локальні змінні
    public void run() {
        System.out.println("T1 is started");

        // todo  //  введення даних
        // todo  //  обчислення функції F1
        // todo E = A + B + C + D*(MA*MD)

        // todo  //  виведення результатів
        System.out.println("T1 is finished");
    }// run

    void F1() {
        // A, B, C are vectors
        // MA, MD are matrices (NxN)
        // MA * MD is matrix multiplication
        // D * MB is multiplication of vector and matrix
    }
}// Потік_F1
