package theodorko

import java.util.*

class ConsoleInput(private val n: Int) : InputOutput {
    override fun enterMatrix(thread: String, name: String): Array<Array<Int>> {
        println("$thread, $name: ")
        val matrix = Array(n) {
            arrayOf(
                n
            )
        }
        for (i in 0 until n) {
            synchronized(sc) {
                for (j in 0 until n) {
                    matrix[i][j] = sc.nextInt()
                }
            }
        }
        println(thread + " " + name + " = " + matrix.contentDeepToString())
        return matrix
    }

    override fun enterVector(thread: String, name: String): IntArray {
        val vector = IntArray(n)
        println("$thread, $name: ")
        synchronized(sc) {
            for (i in 0 until n) {
                vector[i] = sc.nextInt()
            }
        }
        println(thread + " " + name + " = " + vector.contentToString())
        return vector
    }

    override fun print(decorator: String, vector: IntArray) {
        println(decorator + vector.contentToString())
    }

    override fun print(decorator: String, matrix: Array<Array<Int?>?>) {
        println(decorator + matrix.contentDeepToString())
    }

    companion object {
        private val sc = Scanner(System.`in`)
    }
}