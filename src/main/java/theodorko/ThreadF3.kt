package theodorko

class ThreadF3(private val data: Data, private val io: InputOutput) : Thread() {
    override fun run() {
        println("T3 is started")
        val R = io.enterVector("T3", "R")
        val s = io.enterVector("T3", "S")
        try {
            sleep(500)
        } catch (e: InterruptedException) {
            throw RuntimeException(e)
        }
        val mt = io.enterMatrix("T3", "MT")
        val mp = io.enterMatrix("T3", "MP")
        val O = data.func3(R, s, mt, mp)
        io.print("T3 result O = ", O!!)
        println("Task 3, MP")
        println("T3 is finished")
    } // run
} // Потік_F3
