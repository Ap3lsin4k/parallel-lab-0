package theodorko

interface InputOutput {
    fun enterMatrix(thread: String, name: String): Array<Array<Int>>
    fun enterVector(thread: String, name: String): IntArray
    fun print(decorator: String, vector: IntArray)
    fun print(decorator: String, matrix: Array<Array<Int>>)
}