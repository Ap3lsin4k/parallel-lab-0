package theodorko

class ThreadF2(private val data: Data, private val io: InputOutput) : Thread() {
    //  локальні змінні
    override fun run() {
        println("T2 is started")
        val MG = io.enterMatrix("T2", "MG")
        try {
            sleep(500)
        } catch (e: InterruptedException) {
            throw RuntimeException(e)
        }
        val MK = io.enterMatrix("T2", "MK")
        val ML = io.enterMatrix("T2", "ML")
        val MF = data.func2(MG, MK, ML)
        io.print("T2, result MF: ", MF)
        println("T2 is finished")
    } // run
} // Потік_F2
