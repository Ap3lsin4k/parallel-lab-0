package theodorko

import java.util.*

class HomogenousVector(private val n: Int, private val desired: Int) : InputOutput {
    override fun enterMatrix(thread: String, name: String): Array<Array<Int>> {
        println(thread + ", set " + n + "x" + n + " values in " + name + " to " + desired)
        val matrix = Array(n) {
            arrayOf(
                n
            )
        }
        for (i in 0 until n) {
            for (j in 0 until n) {
                matrix[i][j] = desired
            }
        }
        println(thread + " " + name + " = " + Arrays.toString(matrix))
        return matrix
    }

    override fun enterVector(thread: String, name: String): IntArray {
        val vector = IntArray(n)
        println("$thread, $name: ")
        for (i in 0 until n) {
            vector[i] = desired
        }
        println(thread + " " + name + " = " + Arrays.toString(vector))
        return vector
    }

    override fun print(decorator: String, vector: IntArray) {
        println(decorator + Arrays.toString(vector))
        //        Arrays.stream(vector).mapToObj(Integer::toString).forEach(System.out
//                ::println);
    }

    override fun print(decorator: String, matrix: Array<Array<Int>>) {
        println(decorator + Arrays.deepToString(matrix).substring(0, 250) + "...")
        //        Arrays.stream(vector).mapToObj(Integer::toString).forEach(System.out
//                ::println);
    }
}