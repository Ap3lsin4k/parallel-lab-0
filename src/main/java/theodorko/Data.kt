package theodorko

import java.util.*

class Data(private val n: Int) {
    fun func1(
        a: IntArray,
        b: IntArray,
        c: IntArray,
        ma: Array<Array<Int>>,
        md: Array<Array<Int>>,
        d: IntArray
    ): IntArray {
        // todo E = A + B + C + D*(MA*MD)
        // A, B, C are vectors
        // MA, MD are matrices (NxN)
        // MA * MD is matrix multiplication
        // D * MB is multiplication of vector and matrix
        val res: IntArray
        val dmamd = multiply(d, multiply(ma, md))
        res = this.sum(a, sum(b, sum(c, dmamd)))
        return res
    }

    fun func2(mg: Array<Array<Int>>, mk: Array<Array<Int>>, ml: Array<Array<Int>>): Array<Array<Int>> {
        // todo MF = MG*(MK*ML) - MK
        // MG is a matrix
        // MK*ML is matrix multiplication
        // MA - MK is matrix subtraction
        val mult = multiply(mg, multiply(mk, ml))
        return subtract(mult, mk)
    }

    fun func3(r: IntArray, s: IntArray, mt: Array<Array<Int>>, mp: Array<Array<Int>>): IntArray {
        // todo O = SORT(R + S)*(MT*MP)
        // SORT(A) is sorting vector
        // MT * MP is matrix multiplication
        // B * MA is multiplication of vector by matrix
        return sort(multiply(sum(r, s), multiply(mt, mp)))
    }

    fun multiply(ma: Array<Array<Int>>, mb: Array<Array<Int>>): Array<Array<Int>> {
        val c = Array(ma.size) {
            arrayOf(
                mb[0].size
            )
        }
        for (i in ma.indices) {
            for (j in mb[0].indices) {
                c[i][j] = 0
                for (k in mb.indices) {
                    c[i][j] = c[i][j] + ma[i][k] * mb[k][j]
                }
            }
        }
        return c
    }

    private fun multiply(a: IntArray, ma: Array<Array<Int>>): IntArray {
        val res = IntArray(a.size)
        for (i in ma.indices) {
            for (j in ma[0].indices) {
                res[i] += a[j] * ma[j][i]
            }
        }
        return res
    }

    private fun subtract(ma: Array<Array<Int>>, mb: Array<Array<Int>>): Array<Array<Int>> {
        val c = Array(n) {
            arrayOf(
                n
            )
        }
        if (ma.size != n || mb.size != n) {
            return c
        }
        for (i in 0 until n) {
            for (j in 0 until n) {
                c[i][j] = 0
                for (k in 0 until n) {
                    c[i][j] = c[i][j] + (ma[i][k] + mb[k][j])
                }
            }
        }
        return c
    }

    private fun sum(a: IntArray, b: IntArray): IntArray {
        val res = IntArray(n)
        for (i in 0 until n) {
            res[i] = a[i] + b[i]
        }
        return res
    }

    private fun sort(vector: IntArray): IntArray {
        Arrays.sort(vector)
        return vector
    }
}