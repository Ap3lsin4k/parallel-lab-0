package theodorko

// створення  потокового класу для функції  F1
class ThreadF1(private val data: Data, private val io: InputOutput) : Thread() {
    override fun run() {
        println("T1 is started")
        val A: IntArray?
        val B: IntArray?
        val C: IntArray?
        val D: IntArray?
        val MA: Array<Array<Int>>
        val MD: Array<Array<Int>>
        A = io.enterVector("T1", "A")
        B = io.enterVector("T1", "B")
        C = io.enterVector("T1", "C")
        D = io.enterVector("T1", "D")
        MA = io.enterMatrix("T1", "MA")
        MD = io.enterMatrix("T1", "MD")
        val E = data.func1(A, B, C, MA, MD, D)
        io.print("T1 result E:", E!!)
        println("T1 is finished")
    } // run
} // Потік_F1
