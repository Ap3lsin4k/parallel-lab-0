package data

import theodorko.*

/**
 * Паралельне програмування
 * Лабораторна робота №1
 * Федорко Андрій ІО-04
 * Дата 21 09 2022
 * E = A + B + C + D*(MA*MD)
 * O = SORT(R + S)*(MT*MP)
 * MF = MG*(MK*ML) - MK
 *
 */
object Lab0 {
    @JvmStatic
    fun main(args: Array<String>) {
        val n = 1000
        val data = Data(n)
        var io: InputOutput = ConsoleInput(n)
        io = HomogenousVector(n, 1)
        val t1 = ThreadF1(data, io)
        t1.priority = Thread.MIN_PRIORITY
        t1.name = "T1"
        val t2: Thread = ThreadF2(data, io)
        t2.name = "T2"
        t2.priority = Thread.NORM_PRIORITY
        val t3: Thread = ThreadF3(data, io)
        t3.priority = Thread.MAX_PRIORITY
        t2.name = "T3"
        t1.start()
        t2.start()
        t3.start()
        try {
            t1.join()
            t2.join()
            t3.join()
        } catch (e: InterruptedException) {
            throw RuntimeException(e)
        }
    }
}